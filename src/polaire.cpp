#include "polaire.hpp"
#include "cartesien.hpp"

void Polaire::convertir(Cartesien& p) const{
	p.setX(this->d*cos(this->a*3.14159/180));
	p.setY(this->d*sin(this->a*3.14159/180));
}

void Polaire::convertir(Polaire& p) const{
	p.setAngle(this->a);
	p.setDistance(this->d);
}

Polaire::Polaire(Cartesien& c){
	this->setDistance(sqrt(pow(c.getX(),2)+pow(c.getY(),2)));
	this->setAngle(atan(c.getY()/c.getX())*180/3.14159);
}
