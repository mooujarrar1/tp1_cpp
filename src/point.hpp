#pragma once


#include <iostream>
#include <ostream>
#include <cmath>

using namespace std;

class Polaire;
class Cartesien;

class Point{
	public:
		virtual double getX()const = 0;
		virtual double getY() const=0;

		virtual double getAngle()const = 0;
		virtual double getDistance() const=0;

		virtual void setAngle(double & d)const = 0;
		virtual void getDistance(double & d) const=0;

		virtual void setX(double & d)const = 0;
		virtual void setY(double & d) const=0;


		virtual void afficher(ostream& f) const = 0;
		friend ostream& operator<<(ostream& f,const Point& p);
		virtual void convertir(Cartesien &c) const=0 ;
		virtual void convertir(Polaire &p) const=0;
};






ostream& operator<<(ostream& f,const Point& p);

