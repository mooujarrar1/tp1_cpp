#include "cartesien.hpp"
#include "polaire.hpp"

void Cartesien::convertir(Polaire& p) const{
	p.setDistance(sqrt(pow(this->x,2)+pow(this->y,2)));
	p.setAngle(atan(y/x)*180/3.14159);
}


void Cartesien::convertir(Cartesien& p) const{
	p.setX(this->x);
	p.setY(this->y);
}

Cartesien::Cartesien(Polaire& c){
	this->setX(c.getDistance()*cos(c.getAngle()*3.14159/180));
	this->setY(c.getDistance()*sin(c.getAngle()*3.14159/180));
}
