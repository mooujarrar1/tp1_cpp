#include "point.hpp"


class Polaire : public Point{
	private:
		double a;
		double d;
	public:
		Polaire(){
			this->a = 0.0;
			this->d = 0.0;
		}
		Polaire(double a, double d){
			this->a = a;
			this->d = d;
		}
		Polaire(Cartesien& c);
		void setAngle(const double& a){
			this->a = a;
		}
		void setDistance(const double& a){
			this->d = a;
		}
		double getAngle() const{
			return this->a;
		}
		double getDistance() const{
			return this->d;
		}
		void afficher(ostream& f) const{
			f << "(a=" << (int)this->a << ";d=" << (int)this->d << ")";
		}
		void convertir(Polaire& p) const;
		void convertir(Cartesien &c) const;

};


