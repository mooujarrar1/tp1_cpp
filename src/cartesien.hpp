#include "point.hpp"





class Cartesien : public Point{
	private:
		double x;
		double y;
	public:
		Cartesien(){
			this->x = 0.0;
			this->y = 0.0;
		}
		Cartesien(double a, double d){
			this->x = a;
			this->y = d;
		}
		Cartesien(Polaire& c);
		void setX(const double& a){
			this->x = a;
		}
		void setY(const double& a){
			this->y = a;
		}
		double getX() const{
			return this->x;
		}
		double getY() const{
			return this->y;
		}
		void afficher(ostream& f) const{
			f << "(x=" << (int)this->x << ";y=" << (int)this->y << ")";
		}
		void convertir(Polaire& p) const;
		void convertir(Cartesien &c) const;
};




